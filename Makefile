up:
	docker-compose up -d

down:
	docker-compose down

build:
	docker-compose up --build -d

top:
	docker-compose top

test:
	docker exec php-fpm vendor/bin/phpunit --colors=always

perm:
	sudo chown www-data:www-data . -R
	sudo chmod 775 . -R
